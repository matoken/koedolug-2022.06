#!/bin/bash

AUTHER="matoken"

usage_exit() {
        echo "Usage: $0 TITLE SLIDEFILE [CONNPASSID]" 1>&2
        exit 1
}
if [ $# -ne 3 ]; then
  usage_exit
fi

if [ "${1}" == '' ]; then
  usage_exit
fi
SLIDETITLE="${1}"

if [ "${2}" == '' ]; then
  usage_exit
fi
SLIDEFILE="${2}"
THUMBNAIL=$( basename "${SLIDEFILE}" .pdf )
convert -resize 320x320 "${SLIDEFILE}"[0] content/images/"${THUMBNAIL}.jpg"
SLIDEFILE=$( echo "${SLIDEFILE}" | sed -e "s/content\///" )

CONNPASS="${3}"
if [ "${1}" == '' ]; then
  usage_exit
fi
item=$( curl -s "https://connpass.com/api/v1/event/?event_id=${CONNPASS}&format=json" )
RET=$( echo "${item}"   | jq -r .results_returned )
if [ "${RET}" == 0 ]; then
  echo "Could not get data from connpass." 1>&2
  echo "Check network and Connpass ID." 1>&2
  exit 1
fi

TITLE=$(echo "${item}"   | jq -r .events[0].title)
URL=$(echo "${item}"     | jq -r .events[0].event_url)
START=$(echo "${item}"   | jq -r .events[0].started_at | sed -e s/:00+09:00$// -e s/T/\ /)
END=$(echo "${item}"     | jq -r .events[0].ended_at   | sed -e s/:00+09:00$// -e s/T/\ /)
HASHTAG=$(echo "${item}" | jq -r .events[0].hash_tag)
PLACE=$(echo "${item}"   | jq -r .events[0].place)

#echo "TITLE: $TITLE"
#echo "URL: $URL"
#echo "DATE: $START ~ $END"
#echo "https://nitter.matoken.org/search?f=tweets&q=%23$HASHTAG"
#echo "PLACE: $PLACE"

# TITLE: 小江戸らぐ 6月のオフな集まり(第239回)
#URL: https://koedolug.connpass.com/event/248209/
#DATE: 2022-06-11T14:00:00+09:00 ~ 2022-06-11T20:00:00+09:00
#https://nitter.matoken.org/search?f=tweets&q=%23koedolug
#PLACE: オンライン



cat <<__EOT__
= ${SLIDETITLE}

:date: ${START}
:modified: ${START}
:tags: slide, ${HASHTAG}
:category: slide
:slug: 
:authors: ${AUTHER}
:summary: 
:thumbnail: ${THUMBNAIL}.jpg

image:images/${THUMBNAIL}.jpg[]

link:${SLIDEFILE}[pdf]

== ${TITLE}

日時:: $START ~ $END
場所:: $PLACE
URL:: $URL

__EOT__

